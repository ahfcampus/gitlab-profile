# AHFCampus goes Open Source 🚀

Der _AHF Schulverein e.V._ ist Träger mehrerer Schulen in Leipzig und Umgebung, welche gemeinsam den [_AHFCampus_](https://ahfcampus.de) formen.

Auf GitLab werden eigene Software-Projekte verwaltet, dokumentiert und öffentlich zur Verfügung gestellt. Zum Einstieg gibt das Repository [Server Infrastruktur Docs](https://gitlab.com/ahfcampus/server-infrastruktur-docs) einen guten Überblick über den Aufbau der selbst betriebenen IT.

Was heißt _Open Source_? Der Begriff _Open Source_ (zu dt. "offene Quelle"), in der Regel im Kontext von Software verwendet, bedeutet, dass Inhalte (wie z.B. Quelltext) öffentlich sind und von Dritten eingesehen, geändert und genutzt werden können. Dadurch wird [unter anderem](https://de.wikipedia.org/wiki/Open_Source#Vorteile_der_Nutzung) eine Kultur der Zusammenarbeit gefördert, welche der AHFSchulverein e.V. unterstützt.

**DISCLAIMER**: Die hier veröffentlichten Projekte werden nur für die Verwendung an den AHFCampus-Schulen entwickelt, weshalb KEINE Haftung für anderweitigen Gebrauch übernommen werden kann! Es ist stets die ausgewiesene Lizenz zu beachten.

---

### ✉️ Kontakt

- Email: [info@ahfschulverein.de](mailto:info@ahfschulverein.de)
- Telefon: [0341 / 35 26 935](tel:03413526935)

### 🌐 Öffentliche Auftritte

- AHFCampus: [Webseite](https://ahfcampus.de)
- AHFGrundschule Leipzig: [Webseite](https://ahfgrundschule.de) | [Facebook](https://www.facebook.com/AHFGrundschule) | [Instagram](https://www.instagram.com/ahfgrundschule)
- AHFGrundschule Markkleeberg: [Webseite](https://markkleeberg.ahfgrundschule.de)
- AHFOberschule Markkleeberg: [Webseite](https://ahfoberschule.de)

### ❤️ Spenden

- IBAN: DE56 8605 5592 1100 7686 84
- BIC: WELADE8LXXX (Sparkasse Leipzig)
- Empfänger: AHFSchulverein e.V.

---

_Unterstützt durch die [Bildungs Bau e.G.](https://www.bildungs-bau.de/)_